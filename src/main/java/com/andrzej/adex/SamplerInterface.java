package com.andrzej.adex;

import com.andrzej.adex.domain.Request;
import org.apache.spark.api.java.JavaRDD;

/**
 *
 */
public interface SamplerInterface {
    JavaRDD<Request> sampleRequests(JavaRDD<Request> requests);
}
