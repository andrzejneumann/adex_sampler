package com.andrzej.adex;

import com.andrzej.adex.domain.Request;
import com.google.common.collect.Lists;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class SamplerMain {
    public static void main(String[] args) {
        SparkConf conf = new SparkConf().setMaster("local").setAppName("Sampler");
        JavaSparkContext sc = new JavaSparkContext(conf);

        final ArrayList<Request> requests = Lists.newArrayList();
        requests.addAll(generateRequestsForCustomer(1, 10_000));
        requests.addAll(generateRequestsForCustomer(2, 1_000));
        requests.addAll(generateRequestsForCustomer(3, 100_000));
        requests.addAll(generateRequestsForCustomer(4, 20_000));
        requests.addAll(generateRequestsForCustomer(5, 50_000));
        requests.addAll(generateRequestsForCustomer(6, 10_000));
        requests.addAll(generateRequestsForCustomer(7, 200_000));

        final JavaRDD<Request> requestRDD = sc.parallelize(requests);

        SamplerInterface sampler = new Sampler(rdd -> Sampler.countRequestsLocal(rdd, 10_000));

        final JavaRDD<Request> resultRDD = sampler.sampleRequests(requestRDD);

        final Map<Integer, List<Request>> results = resultRDD.collect().stream()
                .collect(Collectors.groupingBy(Request::getCustomerID, Collectors.mapping(r -> r, Collectors.toList())));

        results.entrySet().stream().forEach(entry -> {
            System.out.printf("customerId: %s\n", entry.getKey());
            System.out.printf("\tsample size: %s\n", entry.getValue().size());
        });
    }

    private static List<Request> generateRequestsForCustomer(final int customerId, final int numberOfRequest) {
        return IntStream.range(0, numberOfRequest)
                .mapToObj(i -> generateRequestForCustomer(customerId))
                .collect(Collectors.toList());
    }

    private static Request generateRequestForCustomer(final int customerId) {
        Random rand = new Random();
        return new Request(UUID.randomUUID().toString(),
                rand.nextInt(),
                customerId,
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                rand.nextLong());
    }
}
