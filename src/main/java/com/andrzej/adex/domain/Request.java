package com.andrzej.adex.domain;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 */
public class Request implements Serializable {
    private static final long serialVersionUID = 4865237227953377899L;

    private final String requestID;
    private final int userID;
    private final int customerID;
    private final String userAgent;
    private final String url;
    private final long timestamp;

    public Request(final String requestID,
            final int userID,
            final int customerID,
            final String userAgent,
            final String url,
            final long timestamp) {

        this.requestID = requestID;
        this.userID = userID;
        this.customerID = customerID;
        this.userAgent = userAgent;
        this.url = url;
        this.timestamp = timestamp;
    }

    public String getRequestID() {
        return requestID;
    }

    public int getUserID() {
        return userID;
    }

    public int getCustomerID() {
        return customerID;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public String getUrl() {
        return url;
    }

    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public boolean equals(final Object o) {
        if(this == o) {
            return true;
        }
        if(o == null || getClass() != o.getClass()) {
            return false;
        }
        final Request request = (Request) o;
        return userID == request.userID && customerID == request.customerID && timestamp == request.timestamp &&
                Objects.equals(requestID, request.requestID) && Objects.equals(userAgent, request.userAgent) &&
                Objects.equals(url, request.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(requestID, userID, customerID, userAgent, url, timestamp);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Request{");
        sb.append("requestID='").append(requestID).append('\'');
        sb.append(", userID=").append(userID);
        sb.append(", customerID=").append(customerID);
        sb.append(", userAgent='").append(userAgent).append('\'');
        sb.append(", url='").append(url).append('\'');
        sb.append(", timestamp=").append(timestamp);
        sb.append('}');
        return sb.toString();
    }
}
