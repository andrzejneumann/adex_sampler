package com.andrzej.adex;

import com.andrzej.adex.domain.Request;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import scala.Tuple2;

import java.io.Serializable;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *  Class implementing sapling algorithm.
 */
public class Sampler implements SamplerInterface, Serializable {
    private static final long serialVersionUID = 5013942340185386157L;

    private final CountingMethod countingMethod;

    public Sampler(final CountingMethod countingMethod) {
        this.countingMethod = countingMethod;
    }

    /**
     * Sampler implementation. Uses simple random sampling.
     *
     * @param requests
     * @return
     */
    @Override
    public JavaRDD<Request> sampleRequests(final JavaRDD<Request> requests) {
        final JavaPairRDD<Integer, Request> customerIdToRequestRDD =
                requests.mapToPair(request -> new Tuple2<>(request.getCustomerID(), request));
        final Map<Integer, Double> customerIdToFractionMap = countingMethod.countRequests(customerIdToRequestRDD);

        final JavaPairRDD<Integer, Request> customerIdToExplodedSampleRDD =
                customerIdToRequestRDD.sampleByKey(true, customerIdToFractionMap);

        return customerIdToExplodedSampleRDD.map(p -> p._2());
    }

    /**
     * Method counting request in a approximate, distributed manner. Accuracy is set to 0.1, can be adjusted.
     *
     * @param customerIdToRequestRDD
     * @param sampleSize
     * @return
     */
    public static Map<Integer, Double> countRequestsDistributed(final JavaPairRDD<Integer, Request> customerIdToRequestRDD, final int sampleSize) {
        final JavaPairRDD<Integer, Long> customerIdCountsRDD = customerIdToRequestRDD.countApproxDistinctByKey(0.1);
        return customerIdCountsRDD.mapValues(
                        count -> count <= sampleSize ? 1.0 : (double) sampleSize / (double) count).collectAsMap();
    }

    /**
     * Method counting requests and returning them to the driver.
     *
     * @param customerIdToRequestRDD
     * @param sampleSize
     * @return
     */
    public static Map<Integer, Double> countRequestsLocal(final JavaPairRDD<Integer, Request> customerIdToRequestRDD, final int sampleSize) {
        final Map<Integer, Long> customerIdCountsMap = customerIdToRequestRDD.countByKey();
        return customerIdCountsMap.entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry ->
                        entry.getValue() <= sampleSize ? 1.0 : (double) sampleSize / (double) entry.getValue()));

    }

    @FunctionalInterface
    public interface CountingMethod {
        Map<Integer, Double> countRequests(final JavaPairRDD<Integer, Request> customerIdToRequestRDD);
    }
}
