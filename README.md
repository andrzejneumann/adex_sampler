Implementation notes:


* I included simple test data to illustrate my implementation. SamplerMain class 
can be executed to see example results.

* Implementation depends on request counting. This can be done is approximate 
distributed manner or by just returning counts to the driver.

* Sampling itself is implemented using sampleByKey method from Spark which uses 
Bernoulli sampling.

* Results have size approximately equal sample size. Easy solution if exact result 
is required is to use larger fraction for sampling and then discard results over 
the limit (specified sampling size).

* Map of customerId->fraction has to fit in memory.